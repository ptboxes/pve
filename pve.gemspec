# frozen_string_literal: true

require_relative 'lib/pve/version'

Gem::Specification.new do |spec|
  spec.name          = 'pve'
  spec.version       = PVE::VERSION
  spec.authors       = %w[5xRuby 蒼時弦也]
  spec.email         = ['hi@5xruby.tw', 'contact@frost.tw']

  spec.summary       = 'The ProxmoxVE API Client'
  spec.description   = 'The ProxmoxVE API Client'
  spec.homepage      = 'https://github.com/5xRuby/proxmox-rb'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  # spec.metadata["changelog_uri"] = "TODO"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`
      .split("\x0")
      .reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
