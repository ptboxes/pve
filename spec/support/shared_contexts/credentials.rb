# frozen_string_literal: true

RSpec.shared_context 'has credentials' do
  before do
    body = File.read('spec/fixtures/files/access/ticket/success.json')

    stub_request(:post, %r{example.com/api2/json/access/ticket})
      .to_return(status: 200, body: body, headers: {})
  end

  let(:credentials) do
    PVE::Credentials
      .new('https://example.com', 'example', 'example')
  end
end
