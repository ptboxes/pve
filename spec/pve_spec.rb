# frozen_string_literal: true

RSpec.describe PVE do
  it 'has a version number' do
    expect(PVE::VERSION).not_to be nil
  end

  describe '#credentials=' do
    before { PVE.credentials = 0 }

    it do
      expect { PVE.credentials = 1 }.to(change { PVE.credentials })
    end
  end
end
