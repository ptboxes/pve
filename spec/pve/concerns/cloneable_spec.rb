# frozen_string_literal: true

RSpec.describe PVE::Cloneable do
  include_context 'has credentials'

  let(:body) { File.read('spec/fixtures/files/nodes/qemu/clone/success.json') }
  let(:status) { 200 }

  before do
    stub_request(:post, /example.com/)
      .to_return(status: status, body: body, headers: {})
  end

  let(:dummy_class) do
    Class.new do
      include PVE::Model
      include PVE::Cloneable

      resource '/nodes/%<node_id>s/qemu'
      attribute :id
    end
  end

  # TODO: Create association directly
  let(:model) { dummy_class.new(id: 100, _parameters: { node_id: :pve }) }

  before { PVE.credentials = credentials }

  describe '#clone' do
    subject(:clone) { model.clone(200) }

    it { should_not be_nil }

    context 'when vmid already in used' do
      let(:status) { 500 }

      it { expect { clone }.to raise_error(PVE::IDInUsedError) }
    end
  end
end
