# frozen_string_literal: true

RSpec.describe PVE::Removeable do
  include_context 'has credentials'

  let(:body) { File.read('spec/fixtures/files/nodes/qemu/delete_success.json') }

  before do
    stub_request(:delete, /example.com/)
      .to_return(status: 200, body: body, headers: {})
  end

  let(:dummy_class) do
    Class.new do
      include PVE::Model
      include PVE::Removeable

      resource '/nodes/%<node_id>s/qemu'
      attribute :id
    end
  end

  # TODO: Create association directly
  let(:model) { dummy_class.new(id: 100, _parameters: { node_id: :pve }) }

  before { PVE.credentials = credentials }

  describe '#clone' do
    subject { model.destroy }

    it { should_not be_nil }
  end
end
