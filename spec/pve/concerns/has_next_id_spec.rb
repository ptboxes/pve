# frozen_string_literal: true

RSpec.describe PVE::HasNextID do
  include_context 'has credentials'

  let(:body) { File.read('spec/fixtures/files/cluster/next_id/success.json') }

  before do
    stub_request(:get, /example.com/)
      .to_return(status: 200, body: body, headers: {})
  end

  let(:dummy_class) do
    Class.new do
      include PVE::HasNextID
    end
  end

  before { PVE.credentials = credentials }

  describe '.next_id' do
    subject { dummy_class.next_id }

    it { should eq(101) }

    context 'with invalid response' do
      let(:body) { nil }

      it { should be_nil }
    end
  end
end
