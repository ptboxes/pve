# frozen_string_literal: true

RSpec.describe PVE::Bootable do
  include_context 'has credentials'

  let(:body) { File.read('spec/fixtures/files/nodes/qemu/clone/success.json') }

  before do
    stub_request(:post, /example.com/)
      .to_return(status: 200, body: body, headers: {})
  end

  let(:dummy_class) do
    Class.new do
      include PVE::Model
      include PVE::Bootable

      resource '/nodes/%<node_id>s/qemu'
      attribute :id
    end
  end

  # TODO: Create association directly
  let(:model) { dummy_class.new(id: 100, _parameters: { node_id: :pve }) }

  before { PVE.credentials = credentials }

  describe '#start' do
    subject { model.start }

    it { should_not be_nil }
  end

  describe '#stop' do
    subject { model.stop }

    it { should_not be_nil }
  end

  describe '#reboot' do
    subject { model.reboot }

    it { should_not be_nil }
  end
end
