# frozen_string_literal: true

RSpec.describe PVE::Types::String do
  let(:type) { described_class.new }

  # TODO: Improve test case
  describe '#cast' do
    context 'Number' do
      subject { type.cast(1) }
      it { should be_a(String) }
    end

    context 'Symbol' do
      subject { type.cast(:symbol) }
      it { should be_a(String) }
    end
  end
end
