# frozen_string_literal: true

RSpec.describe PVE::Types::Integer do
  let(:type) { described_class.new }

  # TODO: Improve test case
  describe '#cast' do
    context 'Integer' do
      subject { type.cast(1) }
      it { should be_a(Integer) }
    end

    context 'Float' do
      subject { type.cast(1.5) }
      it { should be_a(Integer) }
    end

    context 'Symbol' do
      subject { type.cast(:symbol) }
      it { should be_nil }
    end
  end
end
