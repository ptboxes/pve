# frozen_string_literal: true

RSpec.describe PVE::Types::Boolean do
  let(:type) { described_class.new }

  describe '#cast' do
    context 'empty string' do
      subject { type.cast('') }
      it { should be_nil }
    end

    context 'string' do
      subject { type.cast('1') }
      it { should be_truthy }
    end

    context 'false value' do
      subject { type.cast(:off) }
      it { should be_falsy }
    end
  end
end
