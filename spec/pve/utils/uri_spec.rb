# frozen_string_literal: true

RSpec.describe PVE::Utils::URI do
  describe '.path' do
    let(:path) { '/nodes' }
    let(:params) { {} }
    subject { described_class.path(path, params) }

    it { should eq("/#{PVE::API_VERSION}/#{PVE::API_FORMAT}/nodes") }

    context 'has duplicate slash' do
      let(:path) { '//nodes' }

      it { should eq("/#{PVE::API_VERSION}/#{PVE::API_FORMAT}/nodes") }
    end

    context 'has params' do
      let(:params) { { full: 1 } }

      it { should eq("/#{PVE::API_VERSION}/#{PVE::API_FORMAT}/nodes?full=1") }
    end
  end
end
