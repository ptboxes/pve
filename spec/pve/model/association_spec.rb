# frozen_string_literal: true

RSpec.describe PVE::Model::Association do
  include_context 'has credentials'

  let(:body) { File.read('spec/fixtures/files/nodes/qemu/success.json') }

  before do
    stub_request(:get, /example.com/)
      .to_return(status: 200, body: body, headers: {})
  end

  let(:dummy_class) do
    Class.new do
      include PVE::Model::Collection
      include PVE::Model::Association

      resource '/nodes/%<node_id>s/qemu'
      primary_key :name

      def initialize(attributes)
        @associations = {}
        @attributes = attributes
      end

      def name
        @attributes['node']
      end
    end
  end

  before { PVE.credentials = credentials }

  describe '.has_many' do
    let(:name) { :machines }
    subject { dummy_class.new({}).respond_to?(name) }

    before do
      dummy_class.has_many(name, class_name: 'PVE::Machine', foreign_key: :node_id)
    end

    it { should be_truthy }

    context 'create new method' do
      subject { dummy_class.new({}).send(name) }

      it { should be_a(PVE::Model::Relation) }
    end
  end

  describe '.has_one' do
    let(:name) { :machine }
    subject { dummy_class.new({}).respond_to?(name) }

    before do
      dummy_class.has_one(name, class_name: 'PVE::Machine', foreign_key: :node_id)
    end

    it { should be_truthy }

    context 'create new method' do
      subject { dummy_class.new({}).send(name) }

      it { should be_a(PVE::Machine) }
    end
  end

  describe '.belongs_to' do
    let(:name) { :node }
    let(:body) { File.read('spec/fixtures/files/nodes/success.json') }
    subject { dummy_class.new({}).respond_to?(name) }

    before do
      dummy_class.belongs_to(name, class_name: 'PVE::Node', foreign_key: :node_id)
    end

    it { should be_truthy }

    context 'create new method' do
      let(:object) { dummy_class.new({}) }
      subject { object.send(name) }

      before do
        object.instance_variable_set(:@associations, node_id: 'pve')
      end

      it { should be_a(PVE::Node) }
    end
  end

  describe '#resource' do
    let(:model) { dummy_class.new({}) }
    subject { model.resource }

    before { model.instance_variable_set(:@associations, node_id: 'pve') }

    it { should eq('/nodes/pve/qemu') }
  end
end
