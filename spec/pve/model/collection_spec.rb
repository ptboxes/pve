# frozen_string_literal: true

RSpec.describe PVE::Model::Collection do
  include_context 'has credentials'

  let(:dummy_class) do
    Class.new do
      include PVE::Model::Collection

      primary_key :name

      def initialize(attributes)
        @attributes = attributes
      end

      def name
        @attributes['node']
      end
    end
  end

  before { PVE.credentials = credentials }

  describe '.resource' do
    before { dummy_class.resource('nodes') }
    subject { dummy_class.resource }

    it { should eq('nodes') }
  end

  describe '.primary_key' do
    before { dummy_class.primary_key(:name) }
    subject { dummy_class.primary_key }

    it { should eq(:name) }
  end

  describe '.foreign_keys' do
    let(:keys) { [:node_id] }
    subject { dummy_class.foreign_keys }

    before do |spec|
      dummy_class.foreign_keys(keys) unless spec.metadata[:skip_before]
    end

    it { should include(:node_id) }

    context 'receive non-array keys', skip_before: true do
      let(:keys) { :node_id }

      it do
        expect { dummy_class.foreign_keys(keys) }.to raise_error(ArgumentError)
      end
    end
  end

  describe '.all' do
    let(:body) { File.read('spec/fixtures/files/nodes/success.json') }
    subject { dummy_class.all }

    before do
      stub_request(:get, /example.com/)
        .to_return(status: 200, body: body, headers: {})
    end

    it { should be_a(PVE::Model::Relation) }
    it { should include(a_kind_of(dummy_class)) }
  end

  describe '.find' do
    let(:body) { File.read('spec/fixtures/files/nodes/success.json') }
    let(:name) { 'pve' }
    subject { dummy_class.find(name) }

    before do
      stub_request(:get, /example.com/)
        .to_return(status: 200, body: body, headers: {})
    end

    it { should be_a(dummy_class) }
    it { should(satisfy { |v| v.name == name }) }
  end
end
