# frozen_string_literal: true

RSpec.describe PVE::Model::Relation do
  include_context 'has credentials'

  before { PVE.credentials = credentials }

  let(:relation) { described_class.new(PVE::Node) }

  describe '#data' do
    let(:body) { File.read('spec/fixtures/files/nodes/success.json') }
    subject { relation.data }

    before do
      stub_request(:get, /example.com/)
        .to_return(status: 200, body: body, headers: {})
    end

    it { should have(1).items }

    context 'is not satisfy' do
      let(:relation) { described_class.new(PVE::Machine) }

      it { should be_empty }
    end
  end

  describe '#where' do
    let(:relation) { described_class.new(PVE::Machine) }
    let(:parameters) { { node_id: 'pve' } }
    subject { relation.parameters }

    before { relation.where(parameters) }

    it { should match(node_id: 'pve') }

    context 'use non foreign key' do
      let(:parameters) { { limit: 1 } }

      it { should be_empty }
      it { expect(relation.queries).to match(limit: 1) }
    end
  end

  describe '#satisfy?' do
    let(:relation) { described_class.new(PVE::Machine) }
    subject { relation.satisfy? }

    it { should be_falsy }

    context 'give PVE::Machine node_id parameters' do
      before { relation.where(node_id: 'pve') }

      it { should be_truthy }
    end
  end
end
