# frozen_string_literal: true

RSpec.describe PVE::Model::Scopable do
  include_context 'has credentials'

  let(:dummy_class) do
    Class.new do
      include PVE::Model::Collection
      include PVE::Model::Scopable
    end
  end
  let(:body) { File.read('spec/fixtures/files/nodes/success.json') }

  before do
    PVE.credentials = credentials
    stub_request(:get, /example.com/)
      .to_return(status: 200, body: body, headers: {})
  end

  describe '#scope' do
    subject { dummy_class }
    before { dummy_class.scope :template, -> {} }

    it { should respond_to(:template) }
    it { expect(subject.scopes).to have_key(:template) }
    it { expect(subject.template).to be_nil }
  end

  describe '#scopes' do
    subject { dummy_class.scopes }

    it { should be_empty }

    context 'add new scope' do
      before { dummy_class.scope :template, -> {} }

      it { should_not be_empty }
    end
  end
end
