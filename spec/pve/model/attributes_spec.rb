# frozen_string_literal: true

RSpec.describe PVE::Model::Attributes do
  let(:dummy_class) do
    Class.new do
      include PVE::Model::Attributes

      attr_accessor :name

      attribute :name
      attribute :version, readonly: true
    end
  end

  let(:klass) { dummy_class.new }

  describe 'defined getter' do
    subject { klass.respond_to?('name') }
    it { should be_truthy }
  end

  describe 'defined setter' do
    subject { klass.respond_to?('name=') }
    it { should be_truthy }

    context 'can set value' do
      before { klass.name = 'Jim' }
      subject { klass.name }
      it { should eq('Jim') }
    end
  end

  describe '#attribute_names' do
    subject { dummy_class.attribute_names }
    it { should include('name') }
  end
end
