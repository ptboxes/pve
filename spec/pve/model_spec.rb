# frozen_string_literal: true

RSpec.describe PVE::Model do
  let(:dummy_class) do
    Class.new do
      include PVE::Model

      attr_accessor :name
    end
  end

  let(:klass) { dummy_class.new(name: 'May') }
  subject { klass.name }
  it { should eq('May') }

  describe '#assign_attributes' do
    before { klass.assign_attributes(name: 'Bob') }
    it { should eq('Bob') }
  end
end
