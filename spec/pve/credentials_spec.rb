# frozen_string_literal: true

RSpec.describe PVE::Credentials do
  let(:node) { 'https://pve.example.com' }
  let(:token_id) { 'example' }
  let(:secret) { 'example' }
  let(:relam) { 'pam' }
  let(:credentials) { described_class.new(node, token_id, secret) }

  describe '#token' do
    subject { credentials.token }

    it { should eq("#{token_id}=#{secret}") }
  end
end
