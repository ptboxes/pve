# frozen_string_literal: true

RSpec.describe PVE::Connection do
  include_context 'has credentials'

  let(:connection) { described_class.new('https://example.com', credentials) }
  let(:status) { 200 }
  let(:response) { File.read('spec/fixtures/files/nodes/success.json') }

  describe '#get' do
    before do
      stub_request(:get, %r{example.com/api2/json/nodes})
        .and_return(status: status, body: response, headers: {})
    end

    subject { connection.get('/nodes') }
    it { should be_a(Net::HTTPOK) }
  end

  describe '#post' do
    let(:body) { {} }

    before do
      stub_request(:post, %r{example.com/api2/json/nodes})
        .and_return(status: status, body: response, headers: {})
    end

    subject { connection.post('/nodes', body) }
    it { should be_a(Net::HTTPOK) }
  end

  describe '#put' do
    let(:body) { {} }

    before do
      stub_request(:put, %r{example.com/api2/json/nodes/pve})
        .and_return(status: status, body: response, headers: {})
    end

    subject { connection.put('/nodes/pve', body) }
    it { should be_a(Net::HTTPOK) }
  end

  describe '#delete' do
    before do
      stub_request(:delete, %r{example.com/api2/json/nodes/pve})
        .and_return(status: status, body: response, headers: {})
    end

    subject { connection.delete('/nodes/pve') }
    it { should be_a(Net::HTTPOK) }
  end

  describe '#request' do
    context 'can set request body' do
      before do
        stub_request(:post, %r{example.com/api2/json/nodes})
          .with(body: { name: 'pve' })
          .and_return(status: status, body: response, headers: {})
      end

      subject { connection.request('Post', '/nodes', {}, { name: 'pve' }) }
      it { should be_a(Net::HTTPOK) }
    end
  end
end
