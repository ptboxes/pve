# frozen_string_literal: true

module PVE
  # The ProxmoxVE CT
  #
  # @since 0.1.0
  # @api private
  class Container
    include PVE::Model
    include PVE::Cloneable
    include PVE::Removeable
    include PVE::Bootable
    include PVE::HasNextID
    include PVE::HasConfig

    # @since 0.1.0
    resource 'nodes/%<node_id>s/lxc'

    # @since 0.1.0
    foreign_keys %i[node_id]

    # @since 0.1.0
    belongs_to :node, class_name: 'PVE::Node',
                      foreign_key: :node_id

    # @since 0.1.0
    # @api public
    attribute :id, 'Integer'
    attribute :name
    attribute :status
    attribute :template, 'Boolean'

    alias vmid= id=

    # @since 0.1.0
    scope :template, -> { select { |item| item.template == true } }
  end
end
