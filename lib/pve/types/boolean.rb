# frozen_string_literal: true

module PVE
  module Types
    # Boolean Caster
    #
    # @since 0.1.0
    class Boolean
      # False values from Rails
      #
      # @since 0.1.0
      FALSE_VALUES = [
        false,
        0,
        '0',
        :"0",
        'f',
        :f,
        'F',
        :F,
        'false',
        false,
        'FALSE',
        :FALSE,
        'off',
        :off,
        'OFF',
        :OFF
      ].to_set.freeze

      # @since 0.1.0
      # @api public
      def cast(value)
        return nil if value == ''

        !FALSE_VALUES.include?(value)
      end
    end
  end
end
