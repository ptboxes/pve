# frozen_string_literal: true

module PVE
  module Types
    # String Caster
    #
    # @since 0.1.0
    class String
      # @since 0.1.0
      # @api public
      def cast(value)
        value.to_s
      end
    end
  end
end
