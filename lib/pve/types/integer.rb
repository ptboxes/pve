# frozen_string_literal: true

module PVE
  module Types
    # Integer Caster
    #
    # @since 0.1.0
    class Integer
      # @since 0.1.0
      # @api public
      def cast(value)
        return nil unless value.respond_to?(:to_i)

        value.to_i
      end
    end
  end
end
