# frozen_string_literal: true

require 'pve/model/attributes'
require 'pve/model/collection'
require 'pve/model/association'
require 'pve/model/scopable'

module PVE
  # Rails-like Model implemention
  #
  # @since 0.1.0
  # @api private
  module Model
    # @since 0.1.0
    # @api private
    def self.included(base)
      base.class_eval do
        include Attributes
        include Collection
        include Association
        include Scopable
      end
    end

    # @param attributes [Hash] the attributes
    #
    # @since 0.1.0
    # @api private
    def initialize(attributes = {})
      @associations = attributes.delete(:_parameters) || {}
      assign_attributes(attributes)
    end

    # Assign attribute by Hash
    #
    # @param attributes [Hash] the hash to assign
    #
    # @since 0.1.0
    def assign_attributes(attributes)
      attributes.each do |key, value|
        setter = "#{key}="
        send(setter, value) if respond_to?(setter)
      end
    end
  end
end
