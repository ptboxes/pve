# frozen_string_literal: true

module PVE
  module Utils
    # URI Helpers
    module URI
      # Format Path
      #
      # @param path [String] the API path
      # @param params [Hash] the API query params
      #
      # @return [String] formatted API path
      def self.path(path, params = {})
        formatted = [API_VERSION, API_FORMAT, path].join('/')
        formatted.gsub!(%r{/+}, '/')
        ["/#{formatted}", ::URI.encode_www_form(params)]
          .reject(&:empty?)
          .join('?')
      end
    end
  end
end
