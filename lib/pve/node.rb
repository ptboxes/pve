# frozen_string_literal: true

require 'pve/model'

module PVE
  # The ProxmoxVE Node
  #
  # @since 0.1.0
  # @api private
  class Node
    include PVE::Model

    # @since 0.1.0
    resource 'nodes'

    # @since 0.1.0
    primary_key :name

    # @since 0.1.0
    # @api public
    attribute :name
    attribute :status

    alias node= name=

    # @since 0.1.0
    has_many :machines, class_name: 'PVE::Machine',
                        foreign_key: :node_id
    has_many :containers, class_name: 'PVE::Container',
                          foreign_key: :node_id
  end
end
