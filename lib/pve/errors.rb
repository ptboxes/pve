# frozen_string_literal: true

module PVE
  # The VMID is already in useed
  class IDInUsedError < StandardError; end
end
