# frozen_string_literal: true

require 'json'

require 'pve/model/findable'

module PVE
  module Model
    # The Object relation
    #
    # @since 0.1.0
    class Relation
      include Enumerable
      include PVE::Model::Findable

      # @since 0.1.0
      attr_reader :parameters, :queries

      # @since 0.1.0
      def initialize(model)
        @model = model
        @parameters = {}
        @queries = {}
      end

      # @since 0.1.0
      def each(&_block)
        data.each do |item|
          yield @model.new(item.merge(_parameters: @parameters))
        end
      end

      # Get the data from API
      #
      # @return [Array|Hash] the result items
      #
      # @since 0.1.0
      def data
        # TODO: Raise error when query not satisfy
        return [] unless satisfy?

        path = format(@model.resource, @parameters)
        @data ||=
          JSON.parse(PVE.connection.get(path, @queries).body).fetch('data')
        @data ||= []
      end

      # Add query parameters
      #
      # @param query [Hash] the queries
      #
      # @since 0.1.0
      def where(query)
        parameters = query.slice(*@model.foreign_keys)
        query.delete_if { |key, _| @model.foreign_keys.include?(key) }
        @parameters.merge!(parameters)
        @queries.merge!(query)
        self
      end

      # Apply scope
      #
      # @param function [Proc] the function to apply
      #
      # @since 0.1.0
      def scope(name)
        scope = @model.scopes[name.to_sym]
        return self if scope.nil?

        instance_exec(&scope)
      end

      # Check for required parameters exists
      #
      # @return [TrueClass|FalseClass]
      #
      # @since 0.1.0
      def satisfy?
        (@model.foreign_keys - @parameters.keys).empty?
      end

      # @since 0.1.0
      def respond_to_missing?(name, include_private = true)
        !@model.scopes[name.to_sym].nil? || super
      end

      # @since 0.1.0
      def method_missing(name, *parameters, &block)
        return super if @model.scopes[name.to_sym].nil?

        scope(name)
      end
    end
  end
end
