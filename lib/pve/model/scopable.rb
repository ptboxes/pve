# frozen_string_literal: true

module PVE
  module Model
    # Create Scope for Model
    #
    # @since 0.1.0
    module Scopable
      # @since 0.1.0
      def self.included(base)
        base.class_eval do
          extend ClassMethods

          @_scopes = {}
        end
      end

      # @since 0.1.0
      module ClassMethods
        # Define Scope
        #
        # @param name [String|Symbol] scope name
        # @param callback [Proc] the callback
        # @param block [Proc] the block if callback not defiend
        #
        # @since 0.1.0
        def scope(name, callback = nil, &block)
          callback ||= block
          @_scopes[name.to_sym] = callback

          define_singleton_method name do
            all.scope(name)
          end
        end

        # Get Scopes
        #
        # @return [Hash] the scopes
        #
        # @since 0.1.0
        def scopes
          @_scopes
        end
      end
    end
  end
end
