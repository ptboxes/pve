# frozen_string_literal: true

require 'pve/types/string'
require 'pve/types/integer'
require 'pve/types/boolean'

module PVE
  module Model
    # Provide Attribute API
    #
    # @since 0.1.0
    # @api private
    module Attributes
      # @since 0.1.0
      # @api private
      def self.included(base)
        base.extend ClassMethods
      end

      # Attribute API
      module ClassMethods
        # Get attributes
        #
        # @return [Array<String>] the list of attribute names
        #
        # @since 0.1.0
        # @api public
        def attribute_names
          (@attributes || {}).keys
        end

        # Define attribute
        #
        # @param name [String|Symbol] the attribute name
        # @param type [String|Class] the attribute type
        #
        # @since 0.1.0
        # @api public
        def attribute(name, type = 'String')
          name = name.to_s
          type = type.to_s

          @attributes ||= {}
          @attributes[name] = type

          define_method(name) { instance_variable_get("@#{name}") }

          define_method("#{name}=") do |value|
            caster = PVE::Types.const_get(type).new
            instance_variable_set("@#{name}", caster.cast(value))
          end
        end
      end
    end
  end
end
