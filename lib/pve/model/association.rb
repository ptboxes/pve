# frozen_string_literal: true

module PVE
  module Model
    # Model Association
    #
    # @since 0.1.0
    module Association
      # @since 0.1.0
      def self.included(base)
        base.class_eval do
          extend ClassMethods
        end
      end

      # Return Resource Path
      #
      # @param options [String] options to override associations
      #
      # @return [String] the api endpoint
      #
      # @since 0.1.0
      def resource(options = {})
        format(self.class.resource, @associations.dup.merge(options.compact))
      end

      # @since 0.1.0
      module ClassMethods
        # rubocop:disable Naming/PredicateName
        # Create many association
        #
        # @param name [String|Symbol] association name
        # @param class_name [String] association model
        # @param foreign_key [String|Symbol] association foreign_key
        #
        # @since 0.1.0
        def has_many(name, class_name:, foreign_key:)
          define_method(name) do
            klass = Kernel.const_get(class_name)
            query = @associations.merge(
              foreign_key.to_sym => send(self.class.primary_key)
            )
            Relation.new(klass).where(query)
          end
        end

        # Create single association
        #
        # @param name [String|Symbol] association name
        # @param class_name [String] association model
        # @param foreign_key [String|Symbol] association foreign_key
        #
        # @since 0.1.0
        def has_one(name, class_name:, foreign_key:)
          define_method(name) do
            klass = Kernel.const_get(class_name)
            query = @associations.merge(
              foreign_key.to_sym => send(self.class.primary_key)
            )
            Relation.new(klass).find_by(query)
          end
        end
        # rubocop:enable Naming/PredicateName

        # Create association to parent
        #
        # @param name [String|Symbol] association name
        # @param class_name [String] association model
        #
        # @since 0.1.0
        def belongs_to(name, class_name:, foreign_key:)
          define_method(name) do
            klass = Kernel.const_get(class_name)
            parameters = @associations.dup
            id = parameters.delete(foreign_key.to_sym)
            Relation.new(klass).where(parameters).find(id)
          end
        end
      end
    end
  end
end
