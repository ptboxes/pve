# frozen_string_literal: true

require 'json'

module PVE
  module Model
    # Find method override
    #
    # @since 0.1.0
    module Findable
      # Find by ID
      #
      # @param id [Number] the object id
      #
      # @return [PVE::Model] target object
      #
      # @since 0.1.0
      def find(id)
        super(nil) do |item|
          item.send(@model.primary_key) == id
        end
      end

      # Find by attribute
      #
      # @param attributes [Hash] the filter attributes
      #
      # @return [PVE::Model] target object
      #
      # @since 0.1.0
      def find_by(**options)
        where(options).first
      end
    end
  end
end
