# frozen_string_literal: true

require 'forwardable'

require 'pve/model/relation'

module PVE
  module Model
    # Add collection interface
    module Collection
      # @since 0.1.0
      # @api private
      def self.included(base)
        base.class_eval do
          extend ClassMethods

          @_resource = ''
          @_foreign_keys = []
          @_primary_key = :id
        end
      end

      # Class Method
      module ClassMethods
        # @since 0.1.0
        extend Forwardable

        # @param path [String] the resource path
        #
        # @return [String]
        #
        # @since 0.1.0
        def resource(path = nil)
          return @_resource if path.nil?

          @_resource = path
        end

        # @param key [String|Symbol] the object primary key
        #
        # @return [String|Symbol]
        #
        # @since 0.1.0
        def primary_key(key = nil)
          return @_primary_key if key.nil?

          @_primary_key = key.to_sym
        end

        # @param keys [Array<Symbol>] the required foreign keys
        #
        # @return [Array<Symbol>]
        #
        # @since 0.1.0
        def foreign_keys(keys = nil)
          return @_foreign_keys if keys.nil?
          raise ArgumentError unless keys.is_a?(Array)

          @_foreign_keys = keys.map(&:to_sym)
        end

        # @return [Array<PVE::Model>] the result sets
        #
        # @since 0.1.0
        def all
          # TODO: Add relation queries
          PVE::Model::Relation.new(self)
        end

        # @since 0.1.0
        delegate %i[first take where find find_by] => :all
      end
    end
  end
end
