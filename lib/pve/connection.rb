# frozen_string_literal: true

require 'net/http'

module PVE
  # The http connection manager
  class Connection
    # @since 0.1.0
    FORM_TYPE_METHODS = %w[Post Put].freeze

    # @param node [String] target node
    # @param credentials [PVE::Credentials] the ProxmoxVE credentials
    #
    # @since 0.1.0
    # @api private
    def initialize(node, credentials)
      @uri = URI(node)
      @credentials = credentials
      @lock = Mutex.new
    end

    # Send HTTP Get Request
    #
    # @param path [String] the path
    # @param params [Hash] the query parameters
    #
    # @since 0.1.0
    # @api public
    def get(path, params = {})
      request('Get', path, params)
    end

    # Send HTTP Post Request
    #
    # @param path [String] the path
    # @param body [Hash] the request body
    #
    # @since 0.1.0
    # @api public
    def post(path, body = {})
      request('Post', path, {}, body)
    end

    # Sent HTTP Put Request
    #
    # @param path [String] the path
    # @param body [Hash] the request body
    #
    # @since 0.1.0
    # @api public
    def put(path, body = {})
      request('Put', path, {}, body)
    end

    # Sent HTTP Delete Request
    #
    # @param path [String] the path
    # @param params [Hash] the query parameters
    #
    # @since 0.1.0
    # @api public
    def delete(path, params = {})
      request('Delete', path, params)
    end

    # Start Request
    #
    # @param method [String] request method
    # @param path [String] request path
    # @param params [Hash] request parameters
    # @param body [Hash] request body
    def request(method, path, params = {}, body = {})
      request = with(method, Utils::URI.path(path, params))
      if FORM_TYPE_METHODS.include?(method)
        request['Content-Type'] = 'application/x-www-form-urlencoded'
        request.body = URI.encode_www_form(body)
      end
      http.start { http.request request }
    end

    private

    # The HTTP Client
    #
    # @since 0.1.0
    # @api private
    def http
      return @http if @http

      @lock.synchronize do
        @http = Net::HTTP.new(@uri.host, @uri.port)
        @http.use_ssl = @uri.scheme == 'https'
        @http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        @http
      end
    end

    # Create an request with credentials
    #
    # @param type [String] http request type
    # @param path [String] http request path
    # @param params [Hash] http request params
    #
    # @return [Net::HTTPRequest] the request
    def with(type, path)
      klass = Net::HTTP.const_get(type)
      request = klass.new(path)
      request['Authorization'] = "PVEAPIToken=#{@credentials.token}"
      request
    end
  end
end
