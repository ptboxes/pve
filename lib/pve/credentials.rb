# frozen_string_literal: true

module PVE
  # The Authentication Credentials
  class Credentials
    # @since 0.1.0
    attr_reader :node, :token_id, :secret

    # @param node [String] the node uri for generate token
    # @param token_id [String] the API Token ID
    # @param password [String] the API Token Secret
    #
    # @since 0.1.0
    # @api private
    def initialize(node, token_id, secret)
      @node = node
      @token_id = token_id
      @secret = secret
    end

    # @return [PVE::Ticket] the API token
    #
    # @since 0.1.0
    # @api public
    def token
      "#{@token_id}=#{@secret}"
    end

    alias ticket token
  end
end
