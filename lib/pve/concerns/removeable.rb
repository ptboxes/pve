# frozen_string_literal: true

module PVE
  # Let Model Removeable
  module Removeable
    # Remove current model
    #
    # @param options [Hash] The extra options
    #
    # @return [String] The UPID from Proxmox VE
    #
    # @since 0.1.0
    def destroy(options = {})
      node = options.delete(:node)
      path = resource(node_id: node)
      body = { purge: 1 }.merge(options)
      res = PVE.connection.delete("#{path}/#{id}", body)
      JSON.parse(res.body).dig('data')
    end
  end
end
