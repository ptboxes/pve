# frozen_string_literal: true

module PVE
  # Get next available VM ID
  module HasNextID
    def self.included(base)
      base.extend ClassMethods
    end

    # :nodoc:
    module ClassMethods
      def next_id
        res = JSON.parse(PVE.connection.get('cluster/nextid').body)
        res['data'].to_i
      rescue JSON::ParserError
        nil
      end
    end
  end
end
