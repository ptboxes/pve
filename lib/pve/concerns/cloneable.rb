# frozen_string_literal: true

module PVE
  # Let Model Clonable
  module Cloneable
    # Clone from current template
    #
    # @param vmid [String] The new instance VM ID
    # @param options [Hash] The extra options
    #
    # @return [String] The UPID from Proxmox VE
    #
    # @since 0.1.0
    def clone(vmid, options = {})
      node = options.delete(:node)
      path = resource(node_id: node)
      body = options.merge(newid: vmid)
      res = PVE.connection.post("#{path}/#{id}/clone", body)
      raise IDInUsedError unless res.is_a?(Net::HTTPOK)

      JSON.parse(res.body).dig('data')
    end
  end
end
