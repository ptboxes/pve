# frozen_string_literal: true

module PVE
  # Get Config
  module HasConfig
    # Get VM/LXC config
    #
    # @param options [Hash] The extra options
    #
    # @return [Hash] config
    #
    # @since 0.1.0
    def config(options = {})
      node = options.delete(:node)
      path = resource(node_id: node)
      res = PVE.connection.get("#{path}/#{id}/config", options)
      JSON.parse(res.body).dig('data')
    end
  end
end
