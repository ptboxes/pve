# frozen_string_literal: true

module PVE
  # Status Manager
  module Bootable
    # Start Machine
    #
    # @param options [Hash] The extra options
    #
    # @return [String] The UPID from Proxmox VE
    #
    # @since 0.1.0
    def start(options = {})
      node = options.delete(:node)
      path = resource(node_id: node)
      res = PVE.connection.post("#{path}/#{id}/status/start", options)
      JSON.parse(res.body).dig('data')
    end

    # Stop Machine
    #
    # @param options [Hash] The extra options
    #
    # @return [String] The UPID from Proxmox VE
    #
    # @since 0.1.0
    def stop(options = {})
      node = options.delete(:node)
      path = resource(node_id: node)
      res = PVE.connection.post("#{path}/#{id}/status/stop", options)
      JSON.parse(res.body).dig('data')
    end

    # Reboot Machine
    #
    # @param options [Hash] The extra options
    #
    # @return [String] The UPID from Proxmox VE
    #
    # @since 0.1.0
    def reboot(options = {})
      node = options.delete(:node)
      path = resource(node_id: node)
      res = PVE.connection.post("#{path}/#{id}/status/reboot", options)
      JSON.parse(res.body).dig('data')
    end
  end
end
