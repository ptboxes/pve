# frozen_string_literal: true

require 'pve/version'

# The ProxmoxVE API Client
module PVE
  # @since 0.1.0
  API_VERSION = 'api2'

  # @since 0.1.0
  API_FORMAT = 'json'

  require 'pve/utils/uri'
  require 'pve/credentials'
  require 'pve/connection'
  require 'pve/errors'

  # Concerns
  require 'pve/concerns/cloneable'
  require 'pve/concerns/removeable'
  require 'pve/concerns/bootable'
  require 'pve/concerns/has_next_id'
  require 'pve/concerns/has_config'

  # Models
  require 'pve/node'
  require 'pve/machine'
  require 'pve/container'

  # @since 0.1.0
  @_mutex = Mutex.new

  module_function

  # Get current credentials
  #
  # @return [PVE::Credentials] return the credentials
  #
  # @since 0.1.0
  # @api private
  def credentials
    @_mutex.synchronize do
      raise 'Credentials is not defined' unless defined?(@_credentials)

      @_credentials
    end
  end

  # Set credentials
  #
  # @param credentials [PVE::Credentials]
  #
  # @since 0.1.0
  def credentials=(credentials)
    @_mutex.synchronize do
      @_credentials = credentials
    end
  end

  # Get current connection
  #
  # @return [PVE::Connection]
  #
  # @since 0.1.0
  def connection
    # TODO: Reuse connection and allow to specify node
    PVE::Connection.new(credentials.node, credentials)
  end
end
